# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv
from tools.translate import _
import time
import tools
from mx import DateTime
import datetime
from dateutil.relativedelta import relativedelta
import decimal_precision as dp
import netsvc
import pooler
from datetime import datetime, date
#from tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, float_compare




class purchase_order_line(osv.osv):
    _name = "purchase.order.line"
    _inherit = "purchase.order.line"

    _columns = {
        'categ_id': fields.many2one('product.category','Category', required=True, change_default=True,help="Select category for the current product"),
        'type_categ': fields.char('Type Category',size=16),
        #Data of product ticket
        'date': fields.datetime('Date and Hour'),
        'flight_line': fields.many2one('elo.flight.line','Flight Line'),
        'destiny': fields.many2one('elo.origin.destiny','Destiny'),
        'origin': fields.many2one('elo.origin.destiny','Origin'),
        'number_flight': fields.char('No. Flight', size=16),
        'seat': fields.char('Seat', size=16),
        'number_ticket': fields.char('No. Ticket', size=16),
        'passenger_id': fields.many2one('elo.passenger','Passenger'),
        #Data of product room
        'check_out': fields.datetime('CheckOut'),
        'check_in': fields.datetime('CheckIn'),
        'hotel_id':  fields.many2one('elo.hotel','Hotel'),
        'passengers': fields.many2many('elo.passenger','elo_order_line_passenger','order_line_id','passenger_id','Passenger'),




    }

    def onchange_categ(self, cr, uid, ids, categ_id, context=None):
        if categ_id:
            categ = self.pool.get('product.category').browse(cr, uid, categ_id,context)
            return {'value': {'type_categ':categ.type}}
        return {'value': {'type_categ':''}}



purchase_order_line()




