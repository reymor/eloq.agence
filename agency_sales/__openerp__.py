# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Agency Sales',
    'version': '1.0',
    'author': 'Neos S.A.',
    'category': 'Hotel',
    "website": "http://http://www.eloquentia.simart.dtdns.net",
    "depends": ['sale','crm','account_accountant','account_voucher','purchase'],
    "description": """ Modules for Agency Sales """,
    "init_xml": [],
    "update_xml": [
         'view/agency_sales_view.xml',
         'view/agency_purchase_view.xml',
         'data/agency_data.xml'
         ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': False,
    'application': True,
}
